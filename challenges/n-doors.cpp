// N Doors
// https://rosettacode.org/wiki/100_doors

#include <iostream>

int nDoors(const int& numDoors=100)
{
    bool doors[numDoors]{};

    for (int i{1}; i <= numDoors; ++i)
        for (int j{1}; j <= numDoors; ++j)
            if (j % i == 0)
                doors[j-1] = !doors[j-1];

    for (int i{0}; i < numDoors; ++i)
        if (doors[i])
            std::cout << "Door #" << i+1 << " is Open\n";

    return 0;
}

int nDoors_b(const int& numDoors=100)
{
    for (int i{1}; i*i <= numDoors; ++i)
        std::cout << "Door #" << i*i << " is Open\n";

    return 0;
}
