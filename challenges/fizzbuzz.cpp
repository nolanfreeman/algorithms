// FizzBuzz
// https://rosettacode.org/wiki/FizzBuzz

#include <iostream>

int main()
{
    int limit{100};

    for (int i{1}; i < limit+1; ++i) {
        if (i % 3 == 0)
            std::cout << "Fizz";
        if (i % 5 == 0)
            std::cout << "Buzz";
        if (i % 3 != 0 && i % 5 != 0)
            std::cout << i;

        std::cout << '\n';
    }

    return 0;
}
