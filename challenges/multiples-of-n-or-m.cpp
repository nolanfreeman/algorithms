// Multiples of N or M
// https://projecteuler.net/problem=1

int multiplesOfNorM(const int* cases, const int& numCases, const int& limit=1000)
{
    int sum{0};

    for (int i{1}; i < limit; ++i)
    {
        for (int c{0}; c < numCases; ++c)
        {
            if ((i % cases[c]) == 0)
            {
                sum += i;
                break;
            }
        }
    }

    return sum; // 233168
}
