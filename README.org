#+TITLE: Algorithms

* Challenges
- [[./challenges/multiples-of-n-or-m.cpp][Multiples of N or M]]
- [[./challenges/fizzbuzz.cpp][FizzBuzz]]
- [[./challenges/n-doors.cpp][N Doors]]
